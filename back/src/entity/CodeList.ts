import { ConfigList } from "./ConfigList";

export interface CodeList{
    config:ConfigList;
    list:any[]
}
import dotenv from 'dotenv';
dotenv.config();
import server from './server';

const startServer = async() =>{
    try {       
        await server.listen({port:parseInt(process.env.PORT!) || 3001});
    } catch (error) {
        server.log.error(error);
        process.exit(1);
    }
}

startServer();

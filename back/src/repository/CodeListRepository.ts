import { CodeList } from "../entity/CodeList";

export interface CodeListRepository{
    generateCodeList():CodeList;

}
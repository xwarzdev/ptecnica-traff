import { FastifyReply, FastifyRequest } from "fastify";
import { CodeList } from "../entity/CodeList";
import { CodeListGeneratorService } from "../service/CodeListGeneratorService";


export const generateCodelist = async (req:FastifyRequest, res:FastifyReply) => {
    try {
        const codeListGenerator = new CodeListGeneratorService();       
        const codeList:CodeList = codeListGenerator.generateCodeList();
        return res.send(codeList.list);
    } catch (error) {
        req.log.error(error);
        return res.send(error);
    }
}
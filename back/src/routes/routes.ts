import { FastifyInstance, FastifyServerOptions } from 'fastify';
import { generateCodelist } from '../controller/CodeListController';

function routes(instance:FastifyInstance,_opts:FastifyServerOptions, done: any) {

    instance.get('/generateCodeList', generateCodelist)

    done();
}

export default routes;

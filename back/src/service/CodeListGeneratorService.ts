import { CodeList } from "../entity/CodeList";
import activeConfig from '../../configAlgo/activeConfig.json'
import { ConfigList } from "../entity/ConfigList";

export class CodeListGeneratorService {
     generateCodeList(): CodeList {
        const codeList: CodeList =  {
            config:activeConfig,
            list:[]
        };
        codeList.list = this.executeAlgo(codeList.config);        
        return codeList;
    }   
    
    private executeAlgo = (config:ConfigList) =>{
        if(config.pattern === 'numeric'){
            return this.algoNumerico(config)
        }else if(config.pattern === 'alfanumerico'){
            return this.algoAlfabet(config)
        }else{
            return []
        }
    }

    private algoNumerico = (config:ConfigList): Array<string>=>{
        return Array(config.count).fill(0).map((val,i)=> ('00000'+val+i).slice(-5));
    }

    private algoAlfabet =  (config:ConfigList): Array<string>=>{
        const aflb = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789';
        const array = [];
        let alfanumber = '';
        for(let k = 0;k < config.count;k++){
                for (let i = 0; i < 6; i++ ) {
                    alfanumber += aflb.charAt(Math.floor(Math.random() * aflb.length));
                }
                array.push(alfanumber)
                alfanumber = '';
            }
        return array;    
    }
}

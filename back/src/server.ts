import Fastify from 'fastify';


const server = Fastify({logger:true})
.register(import('@fastify/compress'))
.register(import('@fastify/cors'),{methods:['GET','POST','PUT','DELETE']})
.register(import('@fastify/helmet'),{hidePoweredBy:true,xssFilter:true})
.register(import('./routes/routes'));

export default server;

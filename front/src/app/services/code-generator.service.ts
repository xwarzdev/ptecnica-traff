import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environment/enviroment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CodeGeneratorService {

  headers = new HttpHeaders().set('content-type', 'application/json')
  constructor(private _http:HttpClient) { }



  generateCodeList():Observable<string[]>{
    return this._http.get<string[]>(`${environment.urlServer}generateCodeList`,{headers:this.headers});
  }


}

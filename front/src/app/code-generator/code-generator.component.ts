import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { CodeGeneratorService } from '../services/code-generator.service';

@Component({
  selector: 'app-code-generator',
  templateUrl: './code-generator.component.html',
  styleUrls: ['./code-generator.component.scss']
})
export class CodeGeneratorComponent {
  codeList$?:Observable<string[]>;
  constructor(private _codeService:CodeGeneratorService){}

  generateCodeList(){    
    this.codeList$ = this._codeService.generateCodeList();
  }




}
